# frozen_string_literal: true

module OmniAuth
  module Alicloud
    VERSION = '3.0.0'
  end
end
